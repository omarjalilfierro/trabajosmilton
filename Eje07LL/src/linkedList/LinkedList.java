package linkedList;

import java.util.Iterator;

import nodo.Nodo;

public class LinkedList<T> implements Iterable<T>{

	private Nodo<T> sent = null;

		public LinkedList() {
		sent = new Nodo<T>();
		sent.setIndex(-1);
	}
		
	public LinkedList(T value){
		this();
		Nodo<T> tmp = new Nodo<T>(value);
		tmp.setIndex(0);
		sent.setNext(tmp);
	}
	
	
	public void addBegin(Nodo n){
		Nodo<T> temp = sent.getNext();
		sent.setNext(n);
	    n.setNext(temp);	
		reIndex();
	}
	public void addBegin(T d){
		Nodo<T> n = new Nodo(d);
		Nodo<T> temp = sent.getNext();
		sent.setNext(n);
	    n.setNext(temp);	
		reIndex();
	}
	public void addEnd(Nodo n){
		Nodo<T> temp = sent;	
		while(temp.getNext()!=null)
			temp = temp.getNext();
		n.setIndex(temp.getIndex()+1);
		temp.setNext(n);	
	}
	public void addEnd(T d){
		Nodo<T> n = new Nodo(d);
		Nodo<T> temp = sent;	
		while(temp.getNext()!=null)
			temp = temp.getNext();
		n.setIndex(temp.getIndex()+1);
		temp.setNext(n);	
	}
	public void addEndRec(T d){
		Nodo<T> temp = sent;	
		addEndRec(d,temp);
		reIndex();
	}
	private void addEndRec(T d,Nodo n){
		if(n.getNext()== null){
			Nodo ne = new Nodo(d);
			n.setNext(ne);
			return;
		}
		else
			addEndRec(d,n.getNext());
	}
	public Nodo removeRec(T temp){
	      return removeRec(sent,temp);
	}
     private Nodo removeRec(Nodo se,T comp){
		Nodo temp = se;
		
			if (temp.getNext()==null){
				return null;
			}
			if(temp.getNext().getValue().equals(comp)){
					Nodo fin =temp.getNext();
					temp.setNext(temp.getNext().getNext());
					System.out.println(fin.getValue());
					return fin;
				}
				return 	removeRec(temp.getNext(),comp);				
	}
        
     
     public Nodo<T> SearchRec(T temp){
	      return SearchRec(sent,temp);
	}
    private Nodo SearchRec(Nodo<T> se,T comp){
			if (se.getNext()==null){
				return null;
			}
			if(se.getNext().getValue().equals(comp)){
					return se.getNext();
				}
			return 	SearchRec(se.getNext(),comp);
					
	}
	 
     public boolean addAfter(T bus,T nuevo){
    	 	Nodo Temp = SearchRec(bus);
    	 	if(Temp != null){
    	 		Nodo n = new Nodo(nuevo);
    	 		n.setNext(Temp.getNext());
    	 		Temp.setNext(n);
    	 		reIndex();
    	 		return true;
    	 	}
    	 	return false;
     }
     
     
     public boolean addBefore(T bus,T nuevo){
    	 if(sent.getNext() == null)
    		 return false; 
 	 	boolean resu =addBefore(sent,sent.getNext(),bus,nuevo);
 	 	reIndex();
 	 	return resu;
     }
     
     public boolean addBefore(Nodo bef,Nodo act,T bus,T nuevo){
 
    	 	if(act.getValue().equals(bus)){
    	 		Nodo nu = new Nodo(nuevo);
    	 		bef.setNext(nu);
    	 		nu.setNext(act);
    	 		return true;
    	 	}
    	 	else
    	 		if(act.getNext() != null)
    	 			addBefore(act,act.getNext(),bus,nuevo);
    	 
    	 	return false;
    	 	
     }
       
	
	
	public Nodo remove(T bus){
		Nodo temp = sent;
		
			while (temp.getNext()!=null){
				if(temp.getNext().getValue().equals(bus)){
					Nodo fin =temp.getNext();
					temp.setNext(temp.getNext().getNext());
					//System.out.println(fin.getValue());
					return fin;
				}
				temp = temp.getNext();
			}
				return null;	
	}
	public boolean removet(T bus){
		Nodo temp = sent;
		
			while (temp.getNext()!=null){
				if(temp.getNext().getValue().equals(bus)){
					Nodo fin =temp.getNext();
					temp.setNext(temp.getNext().getNext());
					//System.out.println(fin.getValue());
					return true;
				}
				temp = temp.getNext();
			}
				return false;	
	}
	

	
	public void reIndex(){
		Nodo temp = sent;
		int cont = 0;
		while(temp.getNext()!=null){
		temp = temp.getNext();
		temp.setIndex(cont++);
		}
	}
	// imprime la lista de manera recursiva 
	public void print(){
		Nodo temp = sent;
		while(temp.getNext()!=null){
			temp = temp.getNext();
			System.out.println(temp.getValue()+" "+ temp.getIndex());
		}
	}
	public Nodo print(Nodo<T> n){
		System.out.println(n.getValue()+" "+ n.getIndex());
		if(n.getNext() != null)
		    return print(n.getNext());
		else
			return null;
	}
	
	public Nodo<T> getByIndex(int i){
		 Nodo<T> temp = sent;
		 int n=0;
		 while ( n != i ) {
			 if(temp.getNext() != null)
			      temp= temp.getNext();
			n++;
		 }
		 return temp;
	}
	
	// imprime la lista de manera recursiva
	public void printRec(){
		print(sent.getNext());
	}
	public boolean isEmpty(){
	if(sent.getNext() != null)
		return false;
	
		return true;
	}
	 public void  clear(){
		 Nodo temp = sent;
		 while (temp.getNext() != null){
			 Nodo temp2 = temp.getNext();
			 temp.setNext(null);
			 temp = temp2;
		 }
		 System.gc();
	 }
	 public Nodo<T>  getFirst(){
		 return sent.getNext();
	 }
	 public Nodo<T> getLast(){
		 Nodo<T> temp = sent;	
			while(temp.getNext()!=null)
				temp = temp.getNext();
			return temp;
	 }
	 public void removeFirst(){
		remove( getFirst().getValue());
	 }
	 public void removeLast(){
		 remove( getLast().getValue());
	 }
	 
	 public boolean replace (T val , T nu){
		 Nodo<T> temp  = SearchRec(val);
		 if (temp != null){
			 temp.setValue(nu);
		 return true;
		 }
	
		 return false ;
	 }

	 public int indexOf (T value){
		
		 Nodo<T> temp = SearchRec(value);
			 if(temp != null)
			      return (int) temp.getIndex();
			 else return -1;
			
	 }
	 
	 public int size(){
		 return (int) getLast().getIndex();
	 }
	 
	 public boolean removeAfter(T value){
		 Nodo <T> tmp = SearchRec(value);
		 if (tmp.getNext() != null){
			 tmp.setNext(tmp.getNext().getNext());
			 reIndex();
			 return true;
		 }
		 return false;
		 }

	 public boolean removeBefore(T value){
		 Nodo <T> tmp = sent;
		 if (tmp.getNext().getValue().equals(value)) return false;
		 while (tmp.getNext().getNext() != null){
			 if (tmp.getNext().getNext().getValue().equals(value)){
				 tmp.setNext(tmp.getNext().getNext());
				 return true;
			 }
			 tmp = tmp.getNext();			 
		 }
		 return false;
	 }
	 
	 @Override
		public Iterator<T> iterator() {
			
			return new Iterator<T>(){
				int i = 0;
			
				@Override
				public boolean hasNext() {
					
						return getByIndex(i).getNext()!=null ? true : false ;
				
				}

				@Override
				public T next() {
					
						return (T) getByIndex(++i).getValue();
					
				}
			
				
			};
	 }
	
	
	
	
	}
