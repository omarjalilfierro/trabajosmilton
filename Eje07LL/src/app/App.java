package app;



import linkedList.LinkedList;
import nodo.Nodo;

public class App {
	
	public static void main(String[] args) {
		LinkedList<String> names = new LinkedList<>();
		
		names.addBegin("jalil");
		
		names.addBegin("luis");
		
		names.addBegin("f");
		
		names.addBegin("lol");
		
		names.addBegin("alex");
		
		names.addEnd("e");
		
		names.remove("e");
		
		names.replace("alex","no alex");
		
		
		for (String name : names) {
			System.out.println(name);
		}
		
		System.out.println("index of  no alex : "+names.SearchRec("no alex").getIndex());
		
		names.addEnd("fin");
		
		names.addBegin("first");
		
		System.out.println("primer valor: "+names.getFirst().getValue());
		
		System.out.println( "ultimo valor: "+names.getLast().getValue());
		
		names.removeFirst();
		
		names.removeLast();
		
		System.out.println("vacio :"+names.isEmpty());
		
		System.out.println("index of  lol: "+ names.indexOf("lol"));
		
		System.out.println( "tama�o :"+names.size());
		
		names.addAfter("lol", "jojo");
		
		names.addBefore("lol", "j");
		
		names.removeBefore("f");
		
		names.removeAfter("f");
		
		names.clear();
		
		
	}	
}
