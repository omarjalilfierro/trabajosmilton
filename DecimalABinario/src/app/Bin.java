package app;

public class Bin {

	public static int bin(int n){
		int resu = n%2;
		
		if (n <= 1) return resu;
		
		else return (bin((n-resu)/2)*10) + resu;
	}
	
}
