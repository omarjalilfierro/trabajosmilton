package app;

import java.util.Scanner;

public class Main {
	

	static Sales [][] meses; 

	public static void main(String[] args){
		Scanner sn = new Scanner(System.in);
		meses = new Sales[12][]; 
		
		meses[0] = new Sales[31];
		meses[1] = new Sales[28];
		meses[2] = new Sales[31];
		meses[3] = new Sales[30];
		meses[4] = new Sales[31];
		meses[5] = new Sales[30];
		meses[6] = new Sales[31];
		meses[7] = new Sales[31];
		meses[8] = new Sales[30];
		meses[9] = new Sales[31];
		meses[10] = new Sales[30];
		meses[11] = new Sales[31];
		
		for (int i = 0; i < meses.length; i++) {
			for (int j = 0; j < meses[i].length; j++) {
				meses[i][j]= new Sales();
			}
		}
		Sales[] temp = null;
		
		for (int i = 0; i < 12; i++) {
			temp = Qsort(meses[i]);
			for (Sales sales : temp) {
				sales.monto = (int)( Math.random() * 1000 + 1);				
			}
		}
		
		/*addSales(4,25,500);
		addSales(4,24,300);
		addSales(0,0,5200);
		addSales(11,13,200);
		*/
		System.out.println("Digite el numero del mes del cual quiere ver los pagos: ");
		System.out.println("Digite el numero 13 si quiere ver los pagos del a�o ");
		int ent = sn.nextInt();
			
		
		if (ent <= 12){
		System.out.println("Digite el monto a buscar");
		System.out.println(Binario(sn.nextDouble(),temp,0,temp.length-1));
		for (Sales sales : temp) 
			System.out.print(sales.monto + " || ");
		}
		else {
			for (int i = 0; i < 12; i++) {
				temp = Qsort(meses[i]);
				for (Sales sales : temp) {
					System.out.print(sales.monto + " || ");
				}
				System.out.println("\n");
			}
		}
		
	}
	
	public static void addSales(int mes, int dia, double cant){
		Sales venta = new Sales();
		venta.dia = dia;
		venta.mes = mes;
		venta.monto = cant;
		meses[mes][dia] = venta;
	}
	
	public static <T> Sales [] Qsort(Sales [] arr) {  
		Sales [] a =	arr; 
    	long startTime = System.currentTimeMillis();
        quicksort(a,0, a.length-1);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        //System.out.println("El timpo de quick "+ elapsedTime);
        return a;
    }

	public static  <T>   Sales [] quicksort(Sales[] b,int low, int high) {
         int i = low,  j = high;

         Sales pivot =  b[low + (high-low)/2];
            
	    
         while (i <= j) {
           while (b[i].compareTo(pivot) == -1) {
                i++;
            }
             while (b[j].compareTo(pivot) == 1) {
                j--;
            }

            if (i <= j) {
                //exchange
            	Sales temp =  b[i];
            	 b[i]=b[j];
            	 b[j]=temp;

                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(b,low, j);
        if (i < high)
            quicksort(b,i, high);
        
      return b;
        
        
    }
public static int Binario(double dato, Sales arr[], int ini, int fin){
	int pos = 0;
	if (ini <= fin) 					pos = (ini+fin)/2;
	else							    return -1;
	if (arr[pos].compareTo(dato) == 0)  return pos;
	if (arr[pos] .compareTo(dato)== -1 )return Binario(dato, arr,pos+1,fin);
	else								return Binario(dato, arr, ini, pos-1);
	
	
}

}
