package app;

public class Sales {

	double monto;
	int mes;
	int dia;
	
	public double getVenta() {
		return monto;
	}
	public void setVenta(double venta) {
		this.monto = venta;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	
	public int compareTo(Sales arg) {	
		if(this.monto > arg.monto) return 1;
		if(this.monto < arg.monto) return -1;
		return 0;
	}
	public int compareTo(double arg) {
		if(this.monto > arg) return 1;
		if(this.monto < arg) return -1;
		return 0;
	}
	
}
