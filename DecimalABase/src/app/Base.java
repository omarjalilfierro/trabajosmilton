package app;

public class Base {
	
	public static String base(int n, int b){
		String ex [] = {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};
		int resu = n%b;
		
		if (n <= b-1) return "" + ex[resu];
		
		else return "" + base((n-resu)/b,b) + ex[resu];
	}
	
}

